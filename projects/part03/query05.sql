. read data.sql

select h1.name, h1.grade
from Highschooler as h1
where grade not in (
  select h2.grade
  from Highschooler as h2,
  friend as f
  where h1.ID = f.ID1 and h2.ID = f.ID2
);
