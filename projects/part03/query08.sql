.read data.sql



SELECT Highschooler.name, Highschooler.grade
  FROM(
      SELECT Max(a), ID1
      FROM(
          SELECT ID1, COUNT(ID1) as a
          FROM Friend
          GROUP BY ID1), Highschooler
          WHERE ID1 = Highschooler.ID
        ),Highschooler
        WHERE ID1 = Highschooler.ID;
