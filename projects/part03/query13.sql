.read data.sql

select h1.name, h1.grade, h2.name, h2.grade
from Highschooler as h1,
Highschooler as h2,
Likes as l
where h1.ID = l.ID1
and h2.ID = l.ID2
and h2.ID not in (select ID1 from Likes);
