This file demonstrates the changes that we made to fix many of the sql injection
attacks in our example program.

After, we generated the diff from the original version with the command

    git --no-pager diff --no-color > 2018-11-19.md

Next, we improved the syntax coloring by telling the markdown engine that the
code block is a diff

    ```diff
    ... content here
    ```

Finally, we filled in a little intro.

# Changes

Below are the changes:


```diff
diff --git a/notes/2018-11-19-fixed.py b/notes/2018-11-19-fixed.py
index a360058..2dd418b 100755
--- a/notes/2018-11-19-fixed.py
+++ b/notes/2018-11-19-fixed.py
@@ -47,10 +47,9 @@ def show_friends(conn):
         FROM Highschoolers as H1
             inner join Friends on H1.ssn = Friends.ssn1
             inner join Highschoolers as H2 on H2.ssn = Friends.ssn2
-        where H1.name = '{}'
-    """.format(name)
-    print(sql)
-    c.execute(sql)
+        where H1.name = ?
+    """
+    c.execute(sql, (name,))
     for row in c.fetchall():
         print(row[0])
     conn.commit()
@@ -61,10 +60,9 @@ def show_user_by_name(conn):
     sql = """
         SELECT *
         FROM Highschoolers
-        where name = '{}'
-    """.format(name)
-    print(sql)
-    c.execute(sql)
+        where name = ?
+    """
+    c.execute(sql, (name,))
     conn.commit()
     for row in c.fetchall():
         print(row)
@@ -72,13 +70,12 @@ def show_user_by_name(conn):
 def show_user_by_order(conn):
     val = get_input('Ordering Val? > ')
     c = conn.cursor()
-    sql = """
+    sql_pattern = """
         SELECT *
         FROM Highschoolers
-        where ordering = {}
-    """.format(val)
-    print(sql)
-    c.execute(sql)
+        where ordering = ?
+    """
+    c.execute(sql_pattern, (val,))
     conn.commit()
     for row in c.fetchall():
         print(row)
@@ -86,16 +83,17 @@ def show_user_by_order(conn):

 def show_friends_or_likes(conn):
     name = get_input('Name? > ')
-    friend_or_likes = get_input('friends or likes?> ')
+    friend_or_likes = get_input('friends or likes?> ').lower()
+    if friend_or_likes not in {'friends', 'likes'}:
+        raise IOError('invalid option')
     c = conn.cursor()
     sql = """
         SELECT *
         FROM Highschoolers as H1
             inner join {} as K on H1.ssn = K.ssn1
-        where H1.name = '{}'
-    """.format(friend_or_likes, name)
-    print(sql)
-    c.execute(sql)
+        where H1.name = :name
+    """.format(friend_or_likes)
+    c.execute(sql, { 'name': name })
     conn.commit()
     for row in c.fetchall():
         print(row)
@@ -112,11 +110,11 @@ def add_friend(conn):
     sql = """
         INSERT INTO Friends VALUES
             (
-                (SELECT ssn FROM Highschoolers WHERE name = '{}'),
-                (SELECT ssn FROM Highschoolers WHERE name = '{}')
+                (SELECT ssn FROM Highschoolers WHERE name = :n1),
+                (SELECT ssn FROM Highschoolers WHERE name = :n2)
             )
-        """.format(name1, name2)
-    c.executescript(sql)
+        """
+    c.execute(sql, {'n1': name1, 'n2': name2})
     after_count =  c.execute('SELECT count(*) from Friends').fetchone()[0]
     conn.commit()
     print("Success" if before_count != after_count else "Fail")
```
