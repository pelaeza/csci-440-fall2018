drop table if exists FriendCountsCache;
drop TRIGGER if exists incFriendCountsCache;


create table FriendCountsCache(
    ssn text primary key,
    num_friends int
);


CREATE TRIGGER incFriendCountsCache AFTER INSERT
ON Friends
BEGIN
    INSERT OR REPLACE INTO FriendCountsCache
    VALUES(new.ssn1,
        (select distinct count(*)
            from (
                select ssn2 from Friends where ssn1 = new.ssn1 union
                select ssn1 from Friends where ssn2 = new.ssn1
        ))
    );

    INSERT OR REPLACE INTO FriendCountsCache
    VALUES(new.ssn2,
        (select distinct count(*)
            from (
                select ssn2 from Friends where ssn1 = new.ssn2 union
                select ssn1 from Friends where ssn2 = new.ssn2
        ))
    );
END;

-- TODO:
-- * counts are wrong

select * from FriendCountsCache;
insert into Friends values('1', '2');
insert into Friends values('1', '4');
insert into Friends values('3', '2');
insert into Friends values('5', '2');
select * from FriendCountsCache;


-- select src, count(src)
--         from (
--             select ssn1 as src, ssn2 as target from Friends union
--             select ssn2 as src, ssn1 as target from friends
--         )
--         group by src;
