#!/usr/bin/env python

import platform
import random
import sqlite3
import string

NUM_HIGHSCHOOL = 1000
NUM_FRIEND = 10000
NUM_LIKES = 10000
DB_FILE = 'tmp.db'


def create_table(connection):
    c = connection.cursor()
    c.execute('DROP TABLE IF EXISTS Friends')
    c.execute('DROP TABLE IF EXISTS Highschoolers')
    c.execute('DROP TABLE IF EXISTS Passwords')
    c.execute('DROP TABLE IF EXISTS Likes')
    c.execute('DROP INDEX IF EXISTS friends_ssn1_ssn2')
    c.execute('''
        CREATE TABLE Highschoolers(
            ordering INTEGER PRIMARY KEY AUTOINCREMENT,
            ssn text,
            name text)
    ''')
    c.execute('''
        CREATE TABLE Friends(
            ssn1 text,
            ssn2 text)
    ''')
    c.execute('''
        CREATE TABLE Likes(
            ssn1 text,
            ssn2 text)
    ''')
    c.execute('''
        CREATE TABLE Passwords(
            ssn1 text,
            password text)
    ''')

    # c.execute('''CREATE UNIQUE INDEX friends_ssn1_ssn2 ON Friends (ssn1, ssn2)''')

    connection.commit()



def random_str(length=12):
    chars = [random.choice(string.ascii_lowercase) for x in range(length)]
    return ''.join(chars)


def generate_highschoolers(connection, num_to_create):
    c = connection.cursor()
    for i in range(num_to_create):
        name = random_str(5)
        c.execute('INSERT INTO Highschoolers(ssn, name) VALUES (?, ?)',
                ('ssn-'+str(i), name))
        c.execute(
                'INSERT INTO Passwords(ssn1, password) VALUES (?, ?)',
                ('ssn-'+str(i), random_str(12)))
    connection.commit()


def generate_pairs(connection, num_to_create, table):
    c = connection.cursor()
    c.execute('SELECT ssn FROM Highschoolers')
    ids = [x[0] for x in c.fetchall()]
    for i in range(num_to_create):
        pair = random.sample(ids, 2)
        c.execute('INSERT INTO {} VALUES (?, ?)'.format(table), pair)
    connection.commit()


def main():
    conn = sqlite3.connect(DB_FILE)
    create_table(conn)
    generate_highschoolers(conn, NUM_HIGHSCHOOL)
    generate_pairs(conn, NUM_FRIEND, "Friends")
    generate_pairs(conn, NUM_LIKES, "Likes")


if __name__ == '__main__':
    main()
