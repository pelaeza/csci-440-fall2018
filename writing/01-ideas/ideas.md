# Team Members

* Alexander Pelaez
* Ethan Miller


Getting started with SQLite: 

After completion of the “Getting Started With SQLite” tutorial, students will be able to create basic SQLite
queries. Some of the topics covered will include SQLite customization, .read and .open statements, SELECT
statements, insert statements and basic use of the terminal. This tutorial is valuable to new users of SQLite as
it will help them learn the basic syntax and statement structure of SQLite as well as execution of certain
actions in the terminal.

Writing Scripts for SQLite: 

After completion of “Writing Scripts For SQLite” tutorial, students will have the ability to write basic queries
in SQL as well as combine many queries in a text editor to form a script that can be read in SQLite3. This
tutorial will be valuable to students because it will allow students to expand their knowledge and insert mass
amounts of information in a database and perform many different queries rather than executing one line
statements in the terminal. With the ability to add and execute lots of ideas students should be able to start
the beginning of basic database development.


Introduction to Relational Algebra: 

The main goal of this tutorial is to get users familiar with certain terms and algebra statements that they can
execute before actually performing them in SQLite3. By familiarizing themselves with these algebra statements
users will be able to understand what the program is actually doing on the inside level rather than just
memorizing ways to access information. Relational algebra statements will include project, select, rename, join
and also delve into nested relational algebra statements. This tutorial will be valuable to a user because it
will provide them with a solid foundation to start writing queries and scripts in SQLite3.
